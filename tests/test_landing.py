import pytest
from selenium import webdriver
import selenium

@pytest.fixture
def webdriver():
    return selenium.webdriver.Chrome()

def test_landing(webdriver, variables):
    webdriver.get(variables['landing_url'])
    assert 'INSTA.tools' in webdriver.title


