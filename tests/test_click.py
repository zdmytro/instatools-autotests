import pytest
from selenium import webdriver
import selenium

@pytest.fixture
def webdriver():
    return selenium.webdriver.Chrome()

def test_landing(webdriver, variables):
    webdriver.get(variables['landing_url'])
    a = webdriver.find_element_by_xpath(variables['xpath_start_button'])
    a.click()
    assert 'INSTA.tools' in webdriver.title