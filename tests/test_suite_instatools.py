import pytest
from selenium import webdriver
import selenium

class Test_suite_instatools:

    @pytest.fixture
    def webdriver(self):
        return selenium.webdriver.Chrome()

    def test_title(self, webdriver, variables):
        webdriver.get(variables['landing_url'])
        assert 'INSTA.tools' in webdriver.title

    def test_fb_reg(self, webdriver, variables):
        webdriver.get(variables['landing_url'])
        start_button = webdriver.find_element_by_xpath(variables['xpath_start_button'])
        start_button.click()
        assert webdriver.find_element_by_xpath(variables['xpath_fb_reg_button'])

    def test_google_reg(self, webdriver, variables):
        webdriver.get(variables['landing_url'])
        start_button = webdriver.find_element_by_xpath(variables['xpath_start_button'])
        start_button.click()
        assert webdriver.find_element_by_xpath(variables['xpath_google_reg_button'])